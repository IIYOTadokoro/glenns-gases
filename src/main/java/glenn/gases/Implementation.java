package glenn.gases;

import glenn.gases.api.IGGImplementation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;

import java.util.Random;

public class Implementation implements IGGImplementation
{
	@Override
	public ItemStack tryRustItem(ItemStack itemstack, Random random)
	{
		if (itemstack == null)
		{
			return null;
		}

		Item item = itemstack.getItem();
		Item replacementItem = Gases.registry.getRustedItem(item);
		if (replacementItem != null)
		{
			return new ItemStack(replacementItem, itemstack.stackSize, itemstack.getItemDamage());
		}
		else
		{
			if (canRustDamageItem(item))
			{
				if (itemstack.attemptDamageItem(1, random))
				{
					return null;
				}
			}
		}

		if (itemstack.stackSize > 0)
		{
			return itemstack;
		}
		else
		{
			return null;
		}
	}

	@Override
	public boolean canRustDamageItem(Item item)
	{
		String material = null;
		if (item instanceof ItemArmor)
		{
			material = ((ItemArmor) item).getArmorMaterial().toString();
		}
		else if (item instanceof ItemTool)
		{
			material = ((ItemTool) item).getToolMaterialName();
		}
		else if (item instanceof ItemSword)
		{
			material = ((ItemSword) item).getToolMaterialName();
		}

		if (material != null)
		{
			return Gases.registry.isRustableMaterial(material);
		}
		else
		{
			return false;
		}
	}
}
