/**
 * The Gases API, used to interact with Glenn's Gases.
 * http://www.jamieswhiteshirt.com/minecraft/mods/gases/
 */
@API(apiVersion = GGAPI.VERSION, owner = GGAPI.OWNER, provides = GGAPI.PROVIDES)
package glenn.gases.api;

import cpw.mods.fml.common.API;
