package glenn.gases.api;

/**
 * <b>The Gases API</b> <br>
 * <br>
 * <i>This API will work both with and without Glenn's Gases installed. Certain
 * methods will not function properly if the mod is not installed.</i> <br>
 * <ul>
 * <li>You can determine the mod installation state by a query to
 * {@link #isInstalled()}.</li>
 * <li><b>IMPORTANT NOTE: To ensure the API will work properly when the mod is
 * loaded, your mod must have the following added to its
 * {@link cpw.mods.fml.common.Mod Mod} annotation:</b><br>
 * <i>{@link cpw.mods.fml.common.Mod#dependencies
 * dependencies}="after:gasesCore"</i></li>
 * <li>If you want the mod to work only if Glenn's Gases is installed, add the
 * following instead:</br>
 * <i>{@link cpw.mods.fml.common.Mod#dependencies
 * dependencies}="require-after:gasesCore"</i></li> <br>
 * </ul>
 * This piece of software is covered under the LGPL license. Redistribution and
 * modification is permitted.
 * 
 * @author Erlend
 * @author Trent
 */
public class GGAPI
{
	public static final String OWNER = "gases";
	public static final String VERSION = "1.0.0";
	public static final String TARGETVERSION = "1.7.10";
	public static final String PROVIDES = "gasesAPI";

	private static boolean isInstalled = false;

	/**
	 * The actual implementation of Glenn's Gases. This serves as a connection
	 * point between the API and the functionality of the mod. If the mod is not
	 * installed, this will be a dummy implementation.
	 */
	public static IGGImplementation implementation = new DummyImplementation();
	/**
	 * The registry of Glenn's Gases. This serves as a connection point between
	 * the API and the registry of the mod. If the mod is not installed, this
	 * will be a dummy registry.
	 */
	public static IGGRegistry registry = new DummyRegistry();

	/**
	 * Returns true if an implementation of Glenn's Gases is installed. This
	 * method may give false negatives if Glenn's Gases is loaded after this
	 * method is called. See {@link GGAPI}.
	 * 
	 * @return True if Glenn's Gases is installed
	 */
	public static boolean isInstalled()
	{
		return isInstalled;
	}

	/**
	 * Install a Glenn's Gases implementation and registry. Used by the Gases
	 * Framework.
	 * 
	 * @param implementation
	 *            The Glenn's Gases implementation
	 * @param registry
	 *            The Glenn's Gases registry
	 */
	public static void install(IGGImplementation implementation, IGGRegistry registry)
	{
		GGAPI.implementation = implementation;
		GGAPI.registry = registry;
		GGAPI.isInstalled = true;
	}
}
