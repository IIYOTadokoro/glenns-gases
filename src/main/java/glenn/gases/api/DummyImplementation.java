package glenn.gases.api;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.Random;

public class DummyImplementation implements IGGImplementation
{
	@Override
	public ItemStack tryRustItem(ItemStack itemstack, Random random)
	{
		return itemstack;
	}

	@Override
	public boolean canRustDamageItem(Item item)
	{
		return false;
	}
}
