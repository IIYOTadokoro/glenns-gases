package glenn.gases.common.item;

import glenn.gases.common.entity.EntityDust;
import glenn.gases.common.entity.EntityTurquoiseDust;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class ItemTurquoiseDust extends ItemDust
{
	@Override
	public EntityDust getThrownEntity(World world, EntityPlayer player)
	{
		return new EntityTurquoiseDust(world, player);
	}
}
