package glenn.gases.common.item;

import glenn.gases.common.entity.EntityDust;

import java.lang.reflect.Constructor;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public abstract class ItemDust extends Item
{
	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player)
	{
		if (!player.capabilities.isCreativeMode)
		{
			--itemstack.stackSize;
		}

		world.playSoundAtEntity(player, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

		if (!world.isRemote)
		{
			for (int i = 0; i < 50; i++)
			{
				world.spawnEntityInWorld(getThrownEntity(world, player));
			}
		}

		return itemstack;
	}

	public abstract EntityDust getThrownEntity(World world, EntityPlayer player);
}