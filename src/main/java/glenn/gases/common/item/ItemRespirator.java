package glenn.gases.common.item;

import glenn.gasesframework.api.ExtendedGasEffectsBase.EffectType;
import glenn.gasesframework.api.gastype.GasType;
import glenn.gasesframework.api.item.IGasEffectProtector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public abstract class ItemRespirator extends ItemArmor implements IGasEffectProtector
{
	private final String armorTexture;

	public ItemRespirator(ArmorMaterial material, String armorTexture)
	{
		super(material, 0, 0);
		this.armorTexture = armorTexture;
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int armorType, String type)
	{
		return armorTexture;
	}

	@Override
	public boolean getIsRepairable(ItemStack slotOne, ItemStack slotTwo)
	{
		return slotTwo.getItem() == Items.coal;
	}

	protected abstract boolean prevent(EffectType effect);

	@Override
	public boolean protect(EntityLivingBase entity, ItemStack itemstack, int slot, GasType gasType, EffectType effect)
	{
		if (slot == 4 && itemstack.getItemDamage() < itemstack.getMaxDamage())
		{
			return prevent(effect);
		}
		return false;
	}

	@Override
	public ItemStack getItemstackOnProtect(EntityLivingBase entity, ItemStack itemstack, int slot, GasType gasType)
	{
		itemstack.damageItem(1, entity);
		return itemstack.stackSize > 0 ? itemstack : null;
	}
}