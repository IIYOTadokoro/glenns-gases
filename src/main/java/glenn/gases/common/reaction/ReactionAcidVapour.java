package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.PartialGasStack;
import glenn.gasesframework.api.reaction.BlockReaction;
import glenn.gasesframework.api.reaction.environment.IBlockReactionEnvironment;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class ReactionAcidVapour extends BlockReaction
{
	@Override
	public void react(IBlockReactionEnvironment environment)
	{
		Block b = environment.getB();
		if (b == Blocks.water || b == Blocks.flowing_water)
		{
			environment.setA(new PartialGasStack(Gases.gasTypeAcidVapour, environment.getA().partialAmount));
		}
	}
}