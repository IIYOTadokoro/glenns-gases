package glenn.gases.common.reaction;

import glenn.gases.common.entity.EntityFlashSparkFX;
import glenn.gasesframework.api.reaction.EntityReaction;
import glenn.gasesframework.api.reaction.environment.IEntityReactionEnvironment;

public class ReactionSparkIgnition extends EntityReaction
{
	@Override
	public void react(IEntityReactionEnvironment environment)
	{
		if (environment.getB() instanceof EntityFlashSparkFX)
		{
			environment.igniteA();
		}
	}
}
