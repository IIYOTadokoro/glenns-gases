package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.reaction.EntityReaction;
import glenn.gasesframework.api.reaction.environment.IEntityReactionEnvironment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

public class ReactionRustItems extends EntityReaction
{
	@Override
	public void react(IEntityReactionEnvironment environment)
	{
		Entity entity = environment.getB();
		if (!entity.worldObj.isRemote)
		{
			if (entity instanceof EntityPlayer)
			{
				EntityPlayer playerEntity = (EntityPlayer) entity;
				InventoryPlayer inventory = playerEntity.inventory;
				for (int i = 0; i < 4; i++)
				{
					ItemStack itemstack = inventory.armorItemInSlot(i);
					inventory.armorInventory[i] = Gases.implementation.tryRustItem(itemstack, environment.getRandom());
				}
			}
			else if (entity instanceof EntityItem)
			{
				EntityItem itemEntity = (EntityItem) entity;
				ItemStack itemStack = itemEntity.getEntityItem();
				ItemStack newItemStack = Gases.implementation.tryRustItem(itemStack, environment.getRandom());

				if (newItemStack != null)
				{
					itemEntity.setEntityItemStack(newItemStack);
				}
				else
				{
					itemEntity.setDead();
				}
			}
		}
	}
}
