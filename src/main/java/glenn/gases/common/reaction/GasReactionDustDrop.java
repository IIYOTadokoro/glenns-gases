package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.PartialGasStack;
import glenn.gasesframework.api.gastype.GasType;
import glenn.gasesframework.api.reaction.GasReaction;
import glenn.gasesframework.api.reaction.environment.IGasReactionEnvironment;

import java.util.Random;

import net.minecraft.item.ItemStack;

public class GasReactionDustDrop extends GasReaction
{
	private final GasType bType;

	public GasReactionDustDrop(GasType bType)
	{
		this.bType = bType;
	}

	@Override
	public void react(IGasReactionEnvironment environment)
	{
		PartialGasStack b = environment.getB();

		if (b.gasType == bType)
		{
			environment.setA(new PartialGasStack(GFAPI.gasTypeAir));
			environment.setB(new PartialGasStack(GFAPI.gasTypeAir));

			Random random = environment.getRandom();
			if (random.nextInt(8) == 0)
			{
				environment.dropItem(new ItemStack(random.nextBoolean() ? Gases.items.turquoiseDust : Gases.items.blueDust));
			}
		}
	}
}
