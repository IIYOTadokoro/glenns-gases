package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.reaction.BlockReaction;

import java.util.Random;

import glenn.gasesframework.api.reaction.environment.IBlockReactionEnvironment;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class ReactionCorrosion extends BlockReaction
{
	@Override
	public void react(IBlockReactionEnvironment environment)
	{
		Random random = environment.getRandom();
		if (random.nextInt(10) == 0)
		{
			Block b = environment.getB();
			if (b != Blocks.air && environment.getBHardness() <= Gases.configurations.gases.corrosiveGas.corrosivePower)
			{
				if (random.nextInt(10) == 0)
				{
					environment.breakB();
				}
				else
				{
					environment.setB(Blocks.air);
				}
			}
		}
	}
}