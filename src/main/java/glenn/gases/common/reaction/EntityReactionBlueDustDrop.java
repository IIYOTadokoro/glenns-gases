package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gases.common.entity.EntityDust;
import glenn.gases.common.entity.EntityTurquoiseDust;
import net.minecraft.item.ItemStack;

public class EntityReactionBlueDustDrop extends EntityReactionDustDrop
{
	@Override
	public ItemStack drop(EntityDust entity)
	{
		if (entity instanceof EntityTurquoiseDust)
		{
			return new ItemStack(Gases.items.blueDust);
		}
		else
		{
			return null;
		}
	}
}
