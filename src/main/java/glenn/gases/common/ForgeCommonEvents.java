package glenn.gases.common;

import glenn.gases.Gases;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.PartialGasStack;
import glenn.gasesframework.api.event.PostBlockBreakEvent;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.PlaySoundAtEntityEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class ForgeCommonEvents
{
	@SubscribeEvent
	public void onPostBlockBreak(PostBlockBreakEvent event)
	{
		if (!event.player.capabilities.isCreativeMode)
		{
			PartialGasStack result = null;
			if (Gases.registry.isCoalDustEmissionBlock(event.previousBlock))
			{
				result = new PartialGasStack(Gases.gasTypeCoalDust, Gases.configurations.gases.coalDust.amountOnMine);
			}
			else if (event.previousBlock.getMaterial() == Material.rock)
			{
				result = new PartialGasStack(Gases.gasTypeDust, Gases.configurations.gases.dust.amountOnMine);
			}

			if (result != null)
			{
				GFAPI.implementation.placeGas(event.world, event.x, event.y, event.z, result);
			}
		}
	}

	private EntityLivingBase previousSoundEventEntity;

	@SubscribeEvent
	public void onPlaySoundAtEntity(PlaySoundAtEntityEvent event)
	{
		if (event.entity instanceof EntityLivingBase)
		{
			EntityLivingBase entity = (EntityLivingBase) event.entity;
			if (previousSoundEventEntity != entity)
			{
				World world = event.entity.worldObj;

				int x = MathHelper.floor_double(entity.posX);
				int y = MathHelper.floor_double(entity.posY + entity.getEyeHeight());
				int z = MathHelper.floor_double(entity.posZ);

				GasType type = GFAPI.implementation.getGasType(world, x, y, z);
				if (type == Gases.gasTypeHelium)
				{
					previousSoundEventEntity = entity;

					world.playSoundAtEntity(entity, event.name, event.volume, event.pitch * 5.0f);
					event.setCanceled(true);
				}
			}
			else
			{
				// The sound has already been handled
				previousSoundEventEntity = null;
			}
		}
	}
}
