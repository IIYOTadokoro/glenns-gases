package glenn.gases.common.gastype;

import java.util.Random;

import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import glenn.gasesframework.api.Combustibility;
import glenn.gasesframework.api.gastype.GasType;

public class GasTypeWhisperingFog extends GasType
{
	public GasTypeWhisperingFog(boolean isIndustrial, int gasID, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasID, name, color, opacity, density, combustibility);
	}

	/**
	 * Called randomly on the client when the player is around a gas block of
	 * this type.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param random
	 */
	@Override
	public void randomDisplayTick(World world, int x, int y, int z, Random random)
	{
		if (random.nextInt(60) == 0)
		{
			world.playSound(x, y, z, "gases:effect.gas_whisper", 1.0f, 1.0f, false);
			world.spawnParticle("depthsuspend", x + random.nextFloat(), y + random.nextFloat(), z + random.nextFloat(), 0.0D, 0.0D, 0.0D);
		}
	}
}