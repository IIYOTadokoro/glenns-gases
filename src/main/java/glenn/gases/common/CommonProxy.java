package glenn.gases.common;

import glenn.gases.Gases;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;

/** Proxy parent object. */
public class CommonProxy
{
	/** Registers custom renderers. */
	public void registerRenderers()
	{

	}

	/** Registers event handlers. */
	public void registerEventHandlers()
	{
		MinecraftForge.EVENT_BUS.register(new ForgeCommonEvents());
	}

	public <REQ extends IMessage, REPLY extends IMessage> void registerMessage(Class<? extends IMessageHandler<REQ, REPLY>> messageHandler, Class<REQ> requestMessageType, int discriminator)
	{
		Gases.networkWrapper.registerMessage(messageHandler, requestMessageType, discriminator, Side.SERVER);
	}

	public EntityPlayer getPlayerEntity(MessageContext ctx)
	{
		return ctx.getServerHandler().playerEntity;
	}
}