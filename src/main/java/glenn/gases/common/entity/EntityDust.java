package glenn.gases.common.entity;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public abstract class EntityDust extends EntityFX
{
	private static final ResourceLocation texture = new ResourceLocation("gases:textures/effects/dust.png");

	protected EntityLivingBase thrower;

	public EntityDust(World world)
	{
		super(world);
	}

	public EntityDust(World world, double x, double y, double z)
	{
		super(world, x, y, z);
	}

	public EntityDust(World world, EntityLivingBase thrower)
	{
		this(world);
		this.thrower = thrower;
		setPosition(thrower.posX - (double) (MathHelper.cos(thrower.rotationYaw / 180.0F * (float) Math.PI) * 0.16F), thrower.posY + thrower.getEyeHeight(), thrower.posZ - (double) (MathHelper.sin(thrower.rotationYaw / 180.0F * (float) Math.PI) * 0.16F));

		double rx = rand.nextDouble() - 0.5D;
		double ry = rand.nextDouble() - 0.5D;
		double rz = rand.nextDouble() - 0.5D;
		double n = Math.sqrt(rx * rx + ry * ry + rz * rz) * 2.0D / rand.nextDouble();

		double d = 0.8D;
		this.motionX = (-MathHelper.sin(thrower.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(thrower.rotationPitch / 180.0F * (float) Math.PI) * d) + thrower.motionX + rx / n;
		this.motionZ = (MathHelper.cos(thrower.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(thrower.rotationPitch / 180.0F * (float) Math.PI) * d) + thrower.motionZ + rz / n;
		this.motionY = (-MathHelper.sin((thrower.rotationPitch) / 180.0F * (float) Math.PI) * d) + thrower.motionY + ry / n;

		maxLifetime = 50 + rand.nextInt(50);
	}

	@Override
	public double getScale(float partialTick)
	{
		return 0.1D + (double) (lifetime + partialTick) / 500.D;
	}

	@Override
	public void onUpdate()
	{
		motionX *= 0.9D;
		motionY = motionY * 0.9D + 0.001D;
		motionZ *= 0.9D;

		super.onUpdate();
	}

	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}
}