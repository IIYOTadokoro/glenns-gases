package glenn.gases.init;

import cpw.mods.fml.common.registry.GameRegistry;
import glenn.gases.Gases;
import glenn.gases.common.item.*;
import net.minecraft.item.Item;

import java.lang.reflect.Field;

public class GGItems
{
	public final Item glowstoneShard = new ItemGlowstoneShard().setUnlocalizedName("gg_glowstoneShard").setTextureName("gases:glowstone_shard");
	public final Item gasDetector = new ItemGasDetector().setUnlocalizedName("gg_gasDetector").setCreativeTab(Gases.creativeTab).setTextureName("gases:detector");
	public final Item gasSensitiveChip = new Item().setUnlocalizedName("gg_gasSensitiveChip").setCreativeTab(Gases.creativeTab).setTextureName("gases:chip");
	public final Item turquoiseDust = new ItemTurquoiseDust().setUnlocalizedName("gg_turquoiseDust").setCreativeTab(Gases.creativeTab).setTextureName("gases:dust_turquoise");
	public final Item blueDust = new ItemBlueDust().setUnlocalizedName("gg_blueDust").setCreativeTab(Gases.creativeTab).setTextureName("gases:dust_blue");
	public final Item snapdragon = new ItemSnapdragon().setUnlocalizedName("gg_snapdragon").setCreativeTab(Gases.creativeTab).setTextureName("gases:snapdragon");
	public final Item primitiveRespirator = new ItemPrimitiveRespirator().setUnlocalizedName("gg_primitiveRespirator").setCreativeTab(Gases.creativeTab).setTextureName("gases:respirator_primitive");
	public final Item advancedRespirator = new ItemAdvancedRespirator().setUnlocalizedName("gg_advancedRespirator").setCreativeTab(Gases.creativeTab).setTextureName("gases:respirator_advanced");
	public final Item refinedDiabaline = (ItemRefinedDiabaline) new ItemRefinedDiabaline().setUnlocalizedName("gg_refinedDiabaline").setCreativeTab(Gases.creativeTab).setTextureName("gases:diabaline");
	public final Item ironIngotRusted = new Item().setUnlocalizedName("gg_ironIngotRusted").setCreativeTab(Gases.creativeTab).setTextureName("gases:iron_ingot_rusted");

	public GGItems()
	{
		for (Field field : getClass().getFields())
		{
			if (Item.class.isAssignableFrom(field.getType()))
			{
				try
				{
					GameRegistry.registerItem((Item) field.get(this), field.getName());
				} catch (IllegalAccessException e)
				{
					throw new RuntimeException("Could not register item " + field.getName(), e);
				}
			}
		}
	}
}
