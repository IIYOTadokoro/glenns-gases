package glenn.gases.init;

import java.lang.reflect.Field;

import cpw.mods.fml.common.registry.GameRegistry;
import glenn.gases.Gases;
import glenn.gases.common.block.BlockAsh;
import glenn.gases.common.block.BlockDiabaline;
import glenn.gases.common.block.BlockDiabalineOre;
import glenn.gases.common.block.BlockFogEmitter;
import glenn.gases.common.block.BlockRustedIronOre;
import glenn.gases.common.block.BlockWarmStone;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCompressed;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public class GGBlocks
{
	public final Block rustedIronOre = new BlockRustedIronOre().setHardness(3.0F).setResistance(5.0F).setStepSound(Block.soundTypePiston).setBlockName("gg_rustedIronOre").setCreativeTab(Gases.creativeTab).setBlockTextureName("gases:iron_ore_rusted");
	public final Block rustedIronBlock = new BlockCompressed(MapColor.brownColor).setHardness(5.0F).setResistance(10.0F).setStepSound(Block.soundTypeMetal).setBlockName("gg_rustedIronBlock").setCreativeTab(Gases.creativeTab).setBlockTextureName("gases:iron_block_rusted");
	public final Block warmStone = new BlockWarmStone().setHardness(1.5F).setResistance(10.0F).setStepSound(Block.soundTypePiston).setBlockName("gg_warmStone").setBlockTextureName("gases:stone_warm");
	public final Block diabalineOre = new BlockDiabalineOre(false).setBlockName("gg_diabalineOre").setCreativeTab(Gases.creativeTab).setBlockTextureName("gases:diabaline_ore");
	public final Block diabalineOreGlowing = new BlockDiabalineOre(true).setBlockName("gg_diabalineOreGlowing").setBlockTextureName("gases:diabaline_ore_glowing");
	public final Block diabalineBlock = new BlockDiabaline(false).setBlockName("gg_diabalineBlock").setCreativeTab(Gases.creativeTab).setBlockTextureName("gases:diabaline_block");
	public final Block diabalineBlockGlowing = new BlockDiabaline(true).setBlockName("gg_diabalineBlockGlowing").setBlockTextureName("gases:diabaline_block_glowing");
	public final Block ash = new BlockAsh().setHardness(0.5F).setBlockName("gg_ash").setBlockTextureName("gases:ash");
	public final Block whisperingFogEmitter = new BlockFogEmitter(Material.rock).setBlockUnbreakable().setResistance(6000000.0F).setStepSound(Block.soundTypePiston).setBlockName("gg_fogEmitter").setBlockTextureName("gases:fogEmitter").setCreativeTab(Gases.creativeTab);

	public GGBlocks()
	{
		for (Field field : getClass().getFields())
		{
			if (Block.class.isAssignableFrom(field.getType()))
			{
				try
				{
					GameRegistry.registerBlock((Block) field.get(this), field.getName());
				} catch (IllegalAccessException e)
				{
					throw new RuntimeException("Could not register block " + field.getName(), e);
				}
			}
		}
	}
}
